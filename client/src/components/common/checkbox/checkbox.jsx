import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const Checkbox = ({ isChecked, label, onChange, id }) => (
  <div className={styles.container}>
    <input
      checked={isChecked}
      className={`${styles.switch} ${styles.pointer}`}
      id={id}
      // id="toggle-checkbox"
      onChange={onChange}
      type="checkbox"
    />
    <label className={styles.pointer} htmlFor={id}>{label}</label>
  </div>
);

Checkbox.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
};

export default Checkbox;
